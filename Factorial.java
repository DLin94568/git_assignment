// David Lin? Don't know what to put here for comments
//Code to calculate factorial of a number
import java.util.Scanner;
// Calculates the factorial of a number and displays to System.out
//The main class
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;

      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 // Display error for user selecting negative number
 //Test end case
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
// Calculate and Factorial
//Actual logic for calculating factorial
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
